# powershell 根据源码得到po文件, $sourcePath 要用相对路径
$sourcePath = "D:\odoo17-x64\source\odoo\addons"
$sourcePathEnt = "D:\PjWeb\o17.odooapp.cn\addons_ent"

# 获取所有 po，社区
# D:\odoo17-po\source\odoo\addons
$pofiles = Get-ChildItem $sourcePath -Filter "zh_CN.po" -Recurse

foreach($file in $pofiles)  {
$filename = $file.Name
$relativePath = Split-Path $file.Directory -Parent
$relativeLeaf = Split-Path $file.Directory -leaf
$newRelativePath = $relativePath -replace 'odoo17-x64', 'odoo17-po'
$newFullPath = $newRelativePath + '\' + $relativeLeaf
md -fo $newFullPath
Copy-Item $file.Fullname $newFullPath -Force -Recurse
}

# 获取所有 po，企业
# D:\odoo17e-po\o17.odooapp.cn\addons_ent
$pofiles = Get-ChildItem $sourcePathEnt -Filter "zh_CN.po" -Recurse

foreach($file in $pofiles)  {
$filename = $file.Name
$relativePath = Split-Path $file.Directory -Parent
$relativeLeaf = Split-Path $file.Directory -leaf
$newRelativePath = $relativePath -replace 'PjWeb', 'odoo17e-po'
$newFullPath = $newRelativePath + '\' + $relativeLeaf
md -fo $newFullPath
Copy-Item $file.Fullname $newFullPath -Force -Recurse
}
